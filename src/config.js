const dotenv = require('dotenv')
const fs = require('fs')
const nodePath = require('path')

function parseDeployment (str) {
    return str.split(',').map(s => s.toLowerCase()).filter(s => s !== '')
}

function required (obj, envName, defaultValue) {
    if (!process.env[envName] && !obj[envName] && !defaultValue) {
        console.error('Required config ' + envName)
        process.exit(1)
    }
    if (process.env[envName]) {
        return process.env[envName]
    }
    return obj[envName] ? obj[envName] : defaultValue
}


module.exports = (path) => {
    if (!path) {
        path = nodePath.join(__dirname, '../.env')
    }
    let config = {}
    try {
        config = dotenv.parse(fs.readFileSync(path))
    } catch (err) {
        // its fine
    }
    return {
        botName: required(config, 'BOT_NAME'),
        botId: required(config, 'BOT_ID'),
        botDisplayName: required(config, 'BOT_DISPLAY_NAME'),
        kataaiToken: required(config,'KATAAI_TOKEN'),
        deployIds: parseDeployment(required(config, 'DEPLOYMENT', '')),
        checkInterval: Number(required(config, 'CHECK_INTERVAL', 10)), // in second,
        apiBaseUrl: required(config, 'API_BASE_URL'),
        kataaiBaseUrl: required(config, 'KATAAI_BASE_URL'),
        bankName: required(config, 'BANK_NAME'),
        defaultCarouselImage: required(config, 'DEFAULT_CAROUSEL_IMAGE'),
        defaultCarouselUrl: required(config, 'DEFAULT_CAROUSEL_URL'),
        bankPhoneNumber: required(config, 'BANK_PHONE_NUMBER')
    }
}