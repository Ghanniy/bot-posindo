exports.slugify = function slugify(text) {
    return text.toString().toLowerCase()
        .replace(/\s+/g, '_')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\_\_+/g, '_')         // Replace multiple - with single -
        .replace(/^_+/, '')             // Trim - from start of text
        .replace(/_+$/, '')             // Trim - from end of text
}

exports.group = function group(items, limit) {
    if (items.length == 0) return []
    return [items.slice(0, limit)]
        .concat(group(items.slice(limit, items.length), limit))
}

exports.required = function (envName, defaultValue) {
    if (!process.env[envName] && !defaultValue) {
        console.error('Required config ' + envName)
        process.exit(1)
    }
    return process.env[envName] ? process.env[envName] : defaultValue
}

function parseProduct(rawProducts, parentId) {
    if (parentId === undefined) {
        const product = {
            title: 'Main',
            id: 0,
            keyword: ['produk']
        };
        product.subProducts = parseProduct(rawProducts, product.id);
        return [product];
    }
    const subProducts = rawProducts.filter(p => p.parent_id === parentId);
    return subProducts.map(sp => Object.assign({}, sp, {
        subProducts: parseProduct(rawProducts, sp.id)
    }))
}

exports.parseProduct = parseProduct

exports.promiseTimeout = function(promise, timeout) {
    return new Promise((resolve, reject) => {
        const tId = setTimeout(() => {
            reject(new Error(`Operation timeout: ${timeout} ms`))
        }, timeout)
        promise.then(value => {
            clearTimeout(tId)
            resolve(value)
        })
        
    })
    
}