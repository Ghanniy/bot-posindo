const yaml = require('js-yaml')
const fs = require('fs')
const path = require('path')
const { createFlow: createProductFlow, createNlu: createProductNlu } = require('./product')
const { slugify, parseProduct } = require('./util')
const createApi = require('./api')

const flowsDir = './bot/flows'

let productNLU, Nlu

async function generate(config) {
    const api = createApi(config)
    let [ products, version ] = await Promise.all([
        api.getProducts().then(res => res.data.data),
        api.getCurrentBotVersion()
    ])

    let bot = {
        schema: 'kata.ai/schema/kata-ml/1.0',
        name: config.botName,
        desc: 'Sabrina BRI Bot Helper',
        version: '0.0.0',
        config: {
            botDisplayName: config.botDisplayName,
            bankName: config.bankName,
            bankPhoneNumber: config.bankPhoneNumber,
            apiBaseUrl: config.apiBaseUrl,
            defaultCarouselImage: config.defaultCarouselImage,
            defaultCarouselUrl: config.defaultCarouselUrl,
            maxRecursion: 10,
        },
        id: config.botId,
        flows: {},
        nlus: {},
        methods: {
            "parseTime(message, context, data, options)": {
                "code": "function parseTime(message, context, data, options){let date = new Date(context.$now + 300000); let dateISO = date.toISOString(); let parsedDate = dateISO.split('T'); let parsedTime = parsedDate[1].split('.'); data.parsedTime = parsedDate[0] + ' ' + parsedTime[0]; let dateEnd = new Date(context.$now + 360000); let dateISOEnd = dateEnd.toISOString(); let parsedDateEnd = dateISOEnd.split('T'); let parsedTimeEnd = parsedDateEnd[1].split('.'); data.parsedTimeEnd = parsedDateEnd[0] + ' ' + parsedTimeEnd[0];}",
                "entry": "parseTime"
            },
            "productNLU(message, options, config)": {
                code: `
                function slugify(text) {
                    return text.toString().toLowerCase()
                        .replace(/\\s+/g, '_')
                        .replace(/[^\\w\\-]+/g, '')
                        .replace(/\\_\\_+/g, '_')
                        .replace(/^_+/, '')
                        .replace(/_+$/, '');
                }
                function match (keywords, message) {
                    return keywords.map(k => message.toLowerCase().indexOf(k.toLowerCase()) !== -1)
                        .find(m => m === true) !== undefined;
                }
                function scan (products, message, weight) {
                    if (!products || products.length <= 0) return [];
                    let matches = [];
                    for (let product of products) {
                        if (match(product.keyword, message)) {
                            matches.push([slugify(product.title), weight]);
                        }
                        matches = matches.concat(scan(product.subProducts, message, weight + 1));
                    }
                    return matches;
                }
                function productNLU (msg, opo, opoiki, opomeneh, config) {
                    const products = config.productHierarchy;
                    const matchesProduct = scan(products, msg.content, 0);
                    if (matchesProduct.length > 0) {
                        return matchesProduct.reduce((a, b) => a[1] < b[1] ? b : a)[0];
                    }
                    return [];
                }
                `,
                entry: 'productNLU'
            }
        }
    }

    bot.version = version

    Nlu = parseNlus()
    productNLU = createProductNlu(products)
    bot.nlus = Object.assign({},
        Nlu,
        exceptionNLU(),
        productNLU
    )
    
    // Its bit of a hack
    products.push(mainProduct = {
        id: 0,
        title: 'Produk dan Layanan',
        keyword: ['product', 'produk']
    })
    const productFlows = generateProductFlow(products)

    bot.flows = Object.assign({}, parseFlow(), productFlows)
    // Configs
    // Products
    const productsConfig = generateProductsConfig(products)
    bot.config.products = productsConfig
    bot.config.productHierarchy = parseProduct(products)
    // Push
    const res = await api.pushBot(bot)
    console.log('Pushing bot to version ' + res.data.version)
    return res.data
}

// Parse flow from yaml to js
function parseFlow () {
    return fs.readdirSync(path.join(__dirname, flowsDir)).map(filename => {
        const flowName = filename.substring(0, filename.indexOf('.yml'))
        const flow = yaml.safeLoad(fs.readFileSync(path.join(__dirname, flowsDir, filename)))
        return [flowName, flow]
    }).reduce((agg, flow) => Object.assign(agg, {[flow[0]]: flow[1]}), {})
}

function parseNlus() {
    return yaml.safeLoad(fs.readFileSync(path.join(__dirname, './bot/nlu.yml')))
}

function exceptionNLU() {
    let exceptionKeyword = Object.assign({},
        Nlu.menu.options.keywords,
        Nlu.cancel.options.keywords,
        productNLU.product.options.keywords
    )
    return {
        exception: {
            type: 'keyword',
            options: {
                keywords: exceptionKeyword
            }
        }
    }
}

function generateProductFlow(products) {
    return products.map((product, index, products) => 
        createProductFlow(product, products.filter(p => p.parent_id === product.id))
    ).reduce((agg, flow) => Object.assign(agg, {[flow[0]]: flow[1]}), {})
}

function generateProductNlu(products) {
    return createProductNlu(products)
}

function generateProductsConfig(products) {
    return products.map((product, index, products) => {
        const subProducts = products.filter(p => p.parent_id === product.id)
            .map(sub => Object.assign({}, sub, {
                hasSub: products.find(p => p.parent_id === sub.id) !== undefined
            }))
        return [slugify(product.title), subProducts]
    }
    ).reduce((agg, [productTitle, subProducts]) => Object.assign(agg, {[productTitle]: subProducts}), {})
}

module.exports = generate